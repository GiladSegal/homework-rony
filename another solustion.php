<?php
require "vendor/autoload.php";
$app = new \Slim\App();
$app->get('/customer/{number}', function($request, $response,$args){
    $json = '{"1":"gilad", "2":"olga"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['number'], $array)){
        echo $array[$args['number']];
    }
    else{
        echo "This user does not exist";
    }

});
$app->run();