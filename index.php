<?php
require "vendor/autoload.php";
$app = new \Slim\App();
$app->get('/customers/{number}', function($request, $response,$args){
    $str = file_get_contents('data.json');
    $array = (json_decode($str,true));
    if(array_key_exists($args['number'], $array)){       	
        return $response->write('customers name is '.$array[$args['number']]);
    }
	else{
		return $response->write('sorry! no customer found');
	}

	
    
});
$app->run();

